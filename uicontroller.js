// Author: Markus Kauppinen 2014
// License: Public Domain

var uiController = {

	// MEMBERS
	"letters" : ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 
		'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
	
	// METHODS
	// Run at page's onload event
	"init" : function() {
		this.drawKeyboard();
		this.setKeyboardDisabled(true);
		// 800x480 or less in portrait
		var mediaQuery = window.matchMedia( 
			"screen and (max-width: 800px) and (orientation: portrait)" );
		if( mediaQuery.matches ) {
			// Narrow screen, adjust canvas size
			var canvas = document.getElementById("gameCanvas");
			canvas.width = 150;
			canvas.height = 150;
		}
		// 800x480 or less in landscape
		var mediaQuery = window.matchMedia( 
			"screen and (max-height: 480px) and (orientation: landscape)" );
		if( mediaQuery.matches ) {
			// Narrow screen, adjust canvas size
			var canvas = document.getElementById("gameCanvas");
			canvas.width = 100;
			canvas.height = 100;
		}
		this.resetCanvas();
	},
	// Resets the UI
	"startNewGame" : function() {
		gameEngine.startNewGame();
		this.setKeyboardDisabled(false);
		document.getElementById("currentGuess").innerHTML = 
			gameEngine.currentGuess;
		this.resetCanvas();
		var currentGuess = document.getElementById("currentGuess");
		currentGuess.style.background = "silver";
	},
	// Enables or disabled the virtual keyboard
	"setKeyboardDisabled" : function(state) {
		var buttons = document.getElementById("keyboardDiv").children;
		for (var i = 0; i < buttons.length; i++) {
  			buttons[i].disabled = state;
		}
	},
	// Handles virtual keyboard input
	"handleInput" : function(alphabet) {
		// Enter the alphabet into the game engine
		if( gameEngine.guess(alphabet) == true ) {
			// A correct guess
			document.getElementById("currentGuess").innerHTML = 
			gameEngine.currentGuess; // For Firefox
		}
		else {
			// A failed guess
			// Update the canvas
			this.updateCanvas();
		}
		// Is the game still running?
		if( gameEngine.gameRunning  == true ) {
			// Yes
		}
		else {
			// No
			// Disable all the alphabet buttons
			this.setKeyboardDisabled(true);
			if( gameEngine.gameResult ) {
				var currentGuess = document.getElementById("currentGuess");
				currentGuess.style.background = "green";
			}
			else {
				var currentGuess = document.getElementById("currentGuess");
				currentGuess.style.background = "red";
			}
		}
	},
	// Dynamically creates a virtual keyboard which
	// enters user's guesses into the game engine
	"drawKeyboard" : function(){
		// Fill in the div with separate UI elements for
		// each letter in the alphabets
		var keyboard = document.getElementById("keyboardDiv");
		for (var i = 0; i < this.letters.length; i++) {
    		var button = document.createElement("button");
			button.id = "letter"+this.letters[i];
			button.value = this.letters[i];
			button.innerHTML = this.letters[i];
			button.onclick = function() { 
				uiController.handleInput(this.value);
				this.disabled = true; };    		
  			keyboard.appendChild(button);
		}
	},

	// Advances the canvas graphics by one step
	"updateCanvas" : function() {
		var canvas = document.getElementById("gameCanvas");
		if (canvas.getContext) {
			var ctx = canvas.getContext('2d');
			ctx.beginPath();
			switch( gameEngine.failedGuesses ) {
				case 1:
					// Draw rope
					ctx.moveTo(0.6*canvas.width, 0.067*canvas.height);
  					ctx.lineTo(0.6*canvas.width, 0.204*canvas.height);
					break;
				case 2:
					// Draw head
					ctx.arc(0.6*canvas.width, 0.267*canvas.height, 
						0.067*canvas.width, 0, (Math.PI/180)*360, false);
					break;
				case 3:
					// Draw body
					ctx.moveTo(0.6*canvas.width, 0.333*canvas.height);
  					ctx.lineTo(0.6*canvas.width, 0.6*canvas.height);
					break;
				case 4:
					// Draw left hand
					ctx.moveTo(0.6*canvas.width, 0.367*canvas.height);
  					ctx.lineTo(0.533*canvas.width, 0.533*canvas.height);
					break;
				case 5:
					// Draw right hand
					ctx.moveTo(0.6*canvas.width, 0.367*canvas.height);
  					ctx.lineTo(0.667*canvas.width, 0.533*canvas.height);
					break;
				case 6:
					// Draw left leg
					ctx.moveTo(0.6*canvas.width, 0.6*canvas.height);
  					ctx.lineTo(0.533*canvas.width, 0.767*canvas.height);
					break;
				case 7:
					// Draw right leg
					ctx.moveTo(0.6*canvas.width, 0.6*canvas.height);
  					ctx.lineTo(0.667*canvas.width, 0.767*canvas.height);
					break;
			}
			ctx.stroke();
		}
		else {
			// Can't get the drawing context!	
		}
	},
	// Resets the canvas graphics to the initial state
	"resetCanvas" : function() {
		var canvas = document.getElementById("gameCanvas");
		if (canvas.getContext) {
  			var ctx = canvas.getContext('2d');
  			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.beginPath();
  			ctx.moveTo(0.733*canvas.width, 0.933*canvas.height);
  			ctx.lineTo(0.267*canvas.width, 0.933*canvas.height);
  			ctx.lineTo(0.267*canvas.width, 0.067*canvas.height);
  			ctx.lineTo(0.6*canvas.width, 0.067*canvas.height);
  			ctx.stroke();
  			ctx.moveTo(0.267*canvas.width, 0.2*canvas.height);
  			ctx.lineTo(0.4*canvas.width, 0.067*canvas.height);
  			ctx.stroke();
		} 
		else {
  			// Can't get the drawing context!
		}
	}
};
