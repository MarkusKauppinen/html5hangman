// Author: Markus Kauppinen 2014
// License: Public Domain

var gameEngine = {
	// MEMBER DATA
	// Number of failed quesses
	"failedGuesses" : "0",
	// Player's guess so far
	"currentGuess" : "",
	// The correct word
	"correctWord" : "",
	// List of the characters that the user has alreasy selected
	"guessedCharacters" : "",
	// The word list
	"words" : ["APPLE", "BANANA", "CABBAGE", "CARROT", "CHERRY", "CUCUMBER", "GRAPEFRUIT", 
		"KIWI", "LEMON", "MANDARIN", "ONION", "ORANGE", "POTATO", "PINEAPPLE",
		"PEAR", "RHUBARB", "RUDEBAKER", "TOMATO", "TURNIP", "WATERMELON"],
	// Game in progress
	"gameRunning" : "true",
	// Game result (victory/failure)
	"gameResult" : "",

	// METHODS
	// Initialises a new game
	"startNewGame" : function() {
		this.gameRunning = true;
		this.correctWord = this.chooseRandomWord();
		this.failedGuesses = 0;
		this.currentGuess = this.correctWord;
		var regexp = /./g;
		this.currentGuess = this.currentGuess.replace(regexp, "*");
		this.guessedCharacters = "";
	},
	// Randomizes a word from the word list
	"chooseRandomWord" : function() {
		var size = this.words.length;
		var index = Math.floor(Math.random()*size);
		return this.words[index];
	},
	// Inputs a user entered character into the game engine and
	// returns a boolean which tells if the character was a correct one
	"guess" : function(character) {
		this.guessedCharacters += character;
		var correctLetter = this.correctWord.indexOf(character);
		
		// The character was in the correct word
		if( correctLetter >= 0 ) {
			var searchIndex = 0;
			var lastFoundIndex = 0;
			do {
				searchIndex = this.correctWord.indexOf(
					character, lastFoundIndex);
				lastFoundIndex = searchIndex + 1;
				if( searchIndex != -1 ){
					// Replace the "_" with the correctly guessed letter
					// by splitting the string and placing the letter
					// in the middle
					var temp = 
						this.currentGuess.substring(0, searchIndex) + 
						character + 
						this.currentGuess.substring(searchIndex + 1);
					this.currentGuess = temp;
				}
			} while( searchIndex != -1 );
			
			// If all the correct characters have been
			// quessed, then end the game
			if( this.currentGuess == this.correctWord ) {
				this.gameRunning = false;
				this.gameResult = true;
			}	
			return true;
		}
		else {
			this.failedGuesses++;
			if( this.failedGuesses > 6 ) {
				this.gameRunning = false;
				this.gameResult = false;
			}
			return false;
		}	
	}
}; 